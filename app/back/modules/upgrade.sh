#!/bin/bash

# Create tmp folder
cd  ~/perso/minecraft-for-all
mkdir tmp
cd tmp

#Get last build of BuildTools
wget https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar;

#create new server with new version
java -jar BuildTools.jar --rev latest

#get .jar file and version number
for i in ./spigot-*.jar; do
	cp $i ../srv/spigot.jar

	#get version number
	version=$i
	name="./spigot-"
	ext=".jar"
	version=${version#"$name"}
	version=${version%"$ext"}

	echo $version > version.txt
	cp ./version.txt ../srv/version.txt
done

cd ..
rm -rf tmp
cd srv
