const util = require('util');
const exec = util.promisify(require('child_process').exec);

const getVersion = async () => {
	const cmd = 'cat ../../srv/version.txt';
	try {
		const { error, stdout, stderr } = await exec(cmd);
		if (error) {
			console.log(`\n\nerror: ${error.message}\n\n`);
			return;
		}
		if (stderr) {
			console.log(`\n\nstderr: ${stderr}\n\n`);
			return;
		}
		return stdout;
	} catch (e) {
		console.log('ERROR : ', e);
		return false;
	}
}

const updateVersion = async () => {
	const cmd = './modules/upgrade.sh';
	try {
		const { error } = await exec(cmd);
		if (error) {
			console.log(`\n\nerror: ${error.message}\n\n`);
			return;
		}
		return true;
	} catch (e) {
		console.log('ERROR : ', e);
		return false;
	}
}

module.exports = {
	getVersion,
	updateVersion
}