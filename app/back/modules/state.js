// const { exec } = require("child_process");
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const getState = async () => {
	const cmd = 'netstat -an | grep 25565';
	try {
		const { error, _stdout, stderr } = await exec(cmd);
		if (error) {
			console.log(`\n\nerror: ${error.message}\n\n`);
			return;
		}
		if (stderr) {
			console.log(`\n\nstderr: ${stderr}\n\n`);
			return;
		}
		return true;
	} catch (e) {
		console.log('ERROR : ', e);
		return false;
	}
}

const getPlayers = () => {
	return 0;
}

const start = async () => {
	const cmd = 'tmux send-keys -t mcapp:2 "java -jar -Xms1024M -Xmx2048M spigot.jar nogui" C-m';
	try {
		await exec(cmd);
		return true;
	} catch (e) {
		console.log('ERROR : ', e);
		return false;
	}
}

const stop = async () => {
	const cmd = 'tmux send-keys -t mcapp:2 "stop" C-m';
	try {
		await exec(cmd);
		return true;
	} catch (e) {
		console.log('ERROR : ', e);
		return false;
	}
}

module.exports = {
	getPlayers,
	getState,
	start,
	stop,
}