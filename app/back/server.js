// Require the framework and instantiate it
const fastify = require('fastify')({ logger: true });
const cors = require('fastify-cors');
const { getPlayers, getState, start, stop } = require('./modules/state');
const { getVersion, updateVersion } = require('./modules/version');

fastify.register(cors, { origin: '*' });

fastify.get('/state', async () => getState().then(state => ({ state })));

fastify.get('/players', async () => ({ players : getPlayers() }));

fastify.post('/start', async () => {
  const state = await getState();
  if (state) {
    return {
      res: false,
      msg: 'Serveur déjà lancé'
    }
  }
  return start().then(() => ({ res: true }));
});

fastify.post('/stop', async () => {
  const state = await getState();
  if (!state) {
    return {
      res: false,
      msg: 'Serveur déjà éteint'
    }
  }
  return stop().then(() => ({ res: true }));
});

fastify.get('/version', async () => {
  const version = await getVersion();
  return { res: version }
});

fastify.post('/update', async () => {
  const state = await getState();
  if (state) {
    return {
      res: false,
      msg: 'On se calme ! Le serveur est lancé'
    }
  }
  const version = await updateVersion();
  return { res: version, msg: version ? null : 'Erreur lors de la mise à jour' }
});

// Run the server!
const server = async () => {
  try {
    await fastify.listen(25566, '0.0.0.0');
    fastify.log.info(`server listening on ${fastify.server.address().port}`);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
}
server();
