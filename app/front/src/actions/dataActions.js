import { toast } from 'react-toastify';
import { FETCH_DATA, REQUEST_DONE, SET_COLLECTION, SWITCH_STATE } from '../actionTypes';
import {
  fetchServerPlayers,
  fetchServerState,
  getServerVersion,
  startServer,
  stopServer,
  updateServer
} from '../api/dataApi';
import { ON, PLAYERS, VERSION } from '../constants/dataTypes';

const setServerData = (attribute, value) => ({
  type: SET_COLLECTION,
  payload: {
    attribute,
    value
  }
});

const fetchData = () => ({
  type: FETCH_DATA
});

const switchState = () => ({
  type: SWITCH_STATE
});

const requestDone = () => ({
  type: REQUEST_DONE
})

const toasterParams = {
  autoClose: 3000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  progress: undefined,
};

const sleep = (s) => new Promise(resolve => setTimeout(resolve, 1000 * s));

export const getServerState = () => dispatch => {
  dispatch(fetchData());
  return fetchServerState()
  .then(({ data : { state }}) => {
    dispatch(setServerData(ON, state));
    if (state) {
      dispatch(fetchData());
      return fetchServerPlayers()
        .then(({ data : { players }}) => dispatch(setServerData(PLAYERS, players)));
    }
  });
} 

export const start = () => dispatch => {
  dispatch(switchState());
  return startServer().then(async ({ data : { res, msg }}) => {
    if (res) {
      toast.success('C\'est parti messire !', toasterParams);
      let serverOn = false;
      while (!serverOn) {
        await sleep(2)
        const { data : { state }} = await fetchServerState();
        serverOn = state;
      }
      dispatch(setServerData(ON, serverOn));
      fetchServerPlayers().then(({ data : { players }}) => 
        dispatch(setServerData(PLAYERS, players)));
    } else {
      toast.error(`Brrrrr ! ${msg}`, toasterParams);
      dispatch(getServerState());
    }
  })
  .catch(() => toast.error('Ah, y\'a une couille dans le paté'), toasterParams);
}

export const stop = () => dispatch => {
  dispatch(switchState());
  stopServer().then(async ({ data : { res, msg }}) => {
    if (res) {
      toast.success('On économise de l\'énergie. C\'est excitant ça Bobby !', toasterParams);
      let serverOn = true;
      while (serverOn) {
        await sleep(2)
        const { data : { state }} = await fetchServerState();
        serverOn = state;
      }
      dispatch(setServerData(ON, serverOn));
      fetchServerPlayers().then(({ data : { players }}) => 
        dispatch(setServerData(PLAYERS, players)));
    } else {
      toast.error(`Brrrrr ! ${msg}`, toasterParams);
      dispatch(getServerState());
    }
  })
  .catch(() => toast.error('Ah, y\'a une couille dans le paté'), toasterParams);
}

export const getVersion = () => dispatch => {
  dispatch(fetchData());
  getServerVersion().then(async ({ data : { res }}) => {
    if (res) {
      dispatch(setServerData(VERSION, res.trim()));
    } else {
      dispatch(requestDone());
    }
  })
  .catch(() => toast.error('Ah, y\'a une couille dans le paté'), toasterParams);
}

export const update = () => dispatch => {
  dispatch(switchState());
  toast.success('Ça part en MAJ !', toasterParams);
  updateServer().then(async ({ data : { res, msg }}) => {
    dispatch(requestDone());
    if (res) {
      toast.success('Et BIM ! Ça, c\'est fait.', toasterParams);
      return dispatch(getVersion())
    } else {
      toast.error(`Brrrrr ! ${msg}`, toasterParams);
    }
  })
  .catch(() => toast.error('Ah, y\'a une couille dans le paté'), toasterParams);
}
