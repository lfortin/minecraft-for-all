import { combineReducers } from 'redux';
import collections from './collectionReducer';

export default combineReducers({
  collections
});