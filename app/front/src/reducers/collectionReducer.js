import { FETCH_DATA, RESET_COLLECTION, REQUEST_DONE, SET_COLLECTION, SWITCH_STATE } from '../actionTypes';
import { LOADING, ON, PLAYERS, VERSION } from '../constants/dataTypes';
const initialCollectionState = {};

const initialState = {
  [LOADING]: false,
  [PLAYERS]: 0,
  [ON]: false,
  [VERSION]: '1.0.0'
};

/**
 * This reducer keeps references to certain collections of data
 */
const collectionReducer = (state = initialState, action) => {
  switch (action.type) {
    // @deprecated. Still here for BC.
    case FETCH_DATA:
    case SWITCH_STATE: {
      return {
        ...state,
        [LOADING]: true
      };
    }
    case SET_COLLECTION: {
      const { attribute, value } = action.payload;
      return {
        ...state,
        [LOADING]: false,
        [attribute]: value
      };
    }
    case RESET_COLLECTION: {
      const { attribute } = action.payload;
      return {
        ...state,
        [LOADING]: false,
        [attribute]: { ...initialCollectionState }
      };
    }
    case REQUEST_DONE: {
      return {
        ...state,
        [LOADING]: false
      };
    }
    default: {
      return state;
    }
  }
};

export default collectionReducer;
