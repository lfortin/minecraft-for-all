import React from 'react';
import styled, { keyframes } from 'styled-components';

const animation = keyframes`
	0% {
		top: 8px;
		height: 32px;
	}
	50%, 100% {
		top: 24px;
		height: 16px;
	}
`;

const Dots = styled('div')`
	display: inline-block;
	position: relative;
	width: 160px;

	div {
		display: inline-block;
		position: absolute;
		left: 8px;
		width: 16px;

		background: #999 url('https://i.ibb.co/rb2TWXL/bgbtn.png') center / cover;
		image-rendering: pixelated;
		border: 2px solid #000;
		color: #DDD;
		padding-bottom: .3em;
		@include flex-center-hv;
		box-shadow: inset -2px -4px #0006, inset 2px 2px #FFF7;

		animation: ${animation} 1.2s cubic-bezier(0, 0.5, 0.5, 1) infinite;
	}

	div:nth-child(1) {
		left: 8px;
		animation-delay: -0.48s;
	}

	div:nth-child(2) {
		left: 40px;
		animation-delay: -0.36s;
	}

	div:nth-child(3) {
		left: 72px;
		animation-delay: -0.24s;
	}
	
	div:nth-child(4) {
		left: 104px;
		animation-delay: -0.12s;
	}
	
	div:nth-child(5) {
		left: 136px;
		animation-delay: 0;
	}
`;

const Loader = () => (
	<Dots>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</Dots>
);

export default Loader;
