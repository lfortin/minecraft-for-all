import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { CgArrowUpR } from 'react-icons/cg';
import styled from 'styled-components';
// actions
import { getServerState, getVersion, start, stop, update } from '../actions/dataActions';
// selectors
import { selectLoading, selectPlayers, selectStateOn , selectVersion} from '../selectors/dataSelectors';
// assets
import background from '../assets/imgs/background.jpg';
import activebg from '../assets/imgs/active-bg.jpg';
import player from '../assets/imgs/player.png';
// Components
import Loader from './Loader';

const App = styled('div')`
	background: url(${({state}) => state ? activebg : background});
	background-repeat: no-repeat;
	background-size: cover;
	box-sizing: border-box;
	width: 100%;
	height: 100vh;
	font-family: 'Minecraft';
	text-transform: uppercase;
	color: #DDD;
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;

	p {
		cursor: default
		font-size: 36px;
	}
	span {
		font-size: 24px;
	}
`;

const Button = styled('button')`
	font-family: 'Minecraft';
	text-transform: capitalize;

	height: 50px;
	width: 300px;
	cursor: pointer;
	overflow: hidden;
	white-space: nowrap;
	user-select: none;

	background: #999 url('https://i.ibb.co/rb2TWXL/bgbtn.png') center / cover;
	image-rendering: pixelated;
	border: 2px solid #000;
	color: #DDD;
	padding-bottom: .3em;
	@include flex-center-hv;
	text-shadow: 2px 2px #000A;
	box-shadow: inset -2px -4px #0006, inset 2px 2px #FFF7;

	&:hover {
		background-color: rgba(100, 100, 255, .45);
		text-shadow: 2px 2px #202013CC;
		color: #FFFFA0;
	}
`;

const PlayerContainer = styled('div')`
	width: 100%
	position: absolute;
	display: flex;
	align-items: flex-end;
	margin: auto;
	right: 0;
	bottom: 0;
	left: 0;

	img {
		margin : 0 0 20px 20px;
	}
`;

const LoaderContainer = styled('div')`
	width: 100%
	position: absolute;
	display: flex;
	justify-content: center;
	align-items: center;
	background: rgba(0, 0, 0, 0.3);
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
`;

const UpgradeContainer = styled('div')`
	width: 100%
	height: 150;
	position: absolute;
	display: flex;
	justify-content: flex-end;
	align-items: center;
	font-family: 'Minecraft';
	color: #DDD;
	margin: auto;
	right: 0;
	bottom: 0;
	left: 0;

	span {
		font-size: 24px;
		margin-right: 10px;
	}

	button {
		margin: 0 20px 20px 0;
		width: 60px;
	}
`;

class Home extends Component {
	componentDidMount = () => {
		const { getServerVersion, getState } = this.props;
		getServerVersion();
		getState();
	}

	switch = () => {
		const { state, startServer, stopServer } = this.props;
		return state ? stopServer() : startServer();
	}

	render = () => {
		const { loading, state, players, updateServer, version } = this.props;
		const list = Array.apply(null, Array(players)).map((_x, i) => i);
		return (
			<>
				<App state={state}>
					<p>Le serveur est {state ? 'allume' : 'eteint'}</p>
					<Button onClick={this.switch}>
						<span>{state ? 'eteindre' : 'allumer'}</span>
					</Button>
				</App>
				{state && !!players && (
					<PlayerContainer>
						{list.map((i) => (
							<img key={`player-${i}`} src={player} height={100} alt="player" />
						))}
					</PlayerContainer>
				)}
				<UpgradeContainer>
					<span>v. {version}</span>
					{!state && (
						<Button onClick={updateServer}>
							<CgArrowUpR size={24} color="#DDD" />
						</Button>
					)}
				</UpgradeContainer>
				{loading && (
					<LoaderContainer>
						<Loader />
					</LoaderContainer>
				)}
			</>
		)
	}
};

Home.propTypes = {
	getServerVersion: PropTypes.func.isRequired,
	getState: PropTypes.func.isRequired,
	loading: PropTypes.bool.isRequired,
	players: PropTypes.number.isRequired,
	startServer: PropTypes.func.isRequired,
	state: PropTypes.bool.isRequired,
	stopServer: PropTypes.func.isRequired,
	updateServer: PropTypes.func.isRequired,
	version: PropTypes.string.isRequired
};

const mapState = (state) => ({
	loading: selectLoading(state),
	players: selectPlayers(state),
	state: selectStateOn(state),
	version: selectVersion(state),
});

const mapDispatch = (dispatch) => ({
	getServerVersion: () => dispatch(getVersion()),
	getState: () => dispatch(getServerState()),
	startServer: () => dispatch(start()),
	stopServer: () => dispatch(stop()),
	updateServer: () => dispatch(update())
});

export default connect(mapState, mapDispatch)(Home);
