import { LOADING, ON, PLAYERS, VERSION } from '../constants/dataTypes';

export const selectStateOn = (state) => state.collections[ON];
export const selectPlayers = (state) => state.collections[PLAYERS];
export const selectLoading = (state) => state.collections[LOADING];
export const selectVersion = (state) => state.collections[VERSION];
