import { get, post } from '../utilities/apiUtilities';

export const fetchServerState = () => get({
	url: '/state'
});

export const fetchServerPlayers = () => get({
	url: '/players'
});

export const startServer = () => post({
	url: '/start'
});

export const stopServer = () => post({
	url: '/stop'
});

export const getServerVersion = () => get({
	url: '/version'
});

export const updateServer = () => post({
	url: '/update'
});

