import { createGlobalStyle } from 'styled-components';
import minecraft from './polices/Minecraft.ttf'

export default createGlobalStyle`
	@font-face {
		font-family: 'Minecraft';
		src: url(${minecraft}) format('truetype');
		font-weight: 300;
		font-style: normal;
		font-display: auto;
	}
`;
