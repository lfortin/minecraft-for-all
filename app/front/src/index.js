import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import store from './store';
import './index.css';
import 'react-toastify/dist/ReactToastify.css';
// components
import App from './App';
// services
import * as serviceWorker from './serviceWorker';
// assets
import Fonts from './assets/fonts';

ReactDOM.render(
  <Provider store={store}>
    <Fonts />
    <ToastContainer />
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
