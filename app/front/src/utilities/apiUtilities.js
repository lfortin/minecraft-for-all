import axios from 'axios';
import { backUrl } from '../env/local';

const api = axios.create({
	baseURL: `${backUrl}:25566`
});

const getHeaders = (config) => ({
  ...config.headers,
  'Accept': 'application/json'
});

export const get = (config) => api({
  method: 'GET',
  ...config,
  headers: getHeaders(config)
});

export const post = (config) => api({
  method: 'POST',
  ...config,
  headers: getHeaders(config)
});

export default { get, post };
