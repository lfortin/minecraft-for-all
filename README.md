# Minecraft for all
# Français

# Introduction
Ce projet permet de transformer un ordinateur linux en serveur minecraft. Il contient plusieurs étapes :
	- Héberger le serveur minecraft en lui même.
	- Le rendre disponible aux personnes qui sont hors de votre réseau,  mais aussi à toute les versions (Attention, le vol est illégal, achetez vos jeux).
	- Donner la possibilité d'allumer et d'éteindre facilement le serveur à travers une application web (client + serveur simple).

Pour info, dans mon cas, le serveur est hébergé sur une Raspberry Pi 4 (4GB), ce qui permet d'allouer beaucoup de ressources sur le serveur et ainsi acceuillir plus de joueurs sans surcharger mon PC.
Également, la raspberry étant allumée h24, l'application web pour allumer et éteindre le serveur permet aux autres personnes de jouer sans que je ne doivent m'occuper d'allumer et éteindre le serveur (Cela consomme beaucoup d'énergie. Ma raspberry étant allumée h24, elle consomme relativement peu, mais ça n'est pas le cas quand le serveur tourne).
Ma raspberry étant sous la distribution Raspbian, les commandes que je vais citer sont valables pour les distro Debian.


# Héberger le serveur minecraft
Cette première étape est la plus simple, il existe plein de tutoriaux en ligne pour le faire :
https://minecraft-fr.gamepedia.com/Tutoriels/Configurer_un_serveur_sous_CraftBukkit#Sur_Linux
Il faut d'abord installer la dernière version de java (changer le numéro de jdk pour le dernier) :
	$ sudo apt-get install openjdk-8-jdk
Verifier ensuite la bonne installation de java :
	$ java -version
>openjdk version "11.0.8" 2020-07-14
>OpenJDK Runtime Environment (build 11.0.8+10-post-Ubuntu-0ubuntu120.04)
>OpenJDK 64-Bit Server VM (build 11.0.8+10-post-Ubuntu-0ubuntu120.04, mixed mode, sharing

Séparez votre projet en 2 parties : Je conseille 2 sous dossiers séparés.
	- srv -> Pour le serveur Minecraft lui-même
	- app -> pour créer l'app qui vous permet de contrôler votre serveur à distance (inutile si vous jouez en local)
Télécharger la dernière version de BuildTools dans le dossier "srv".
https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
Lancer :
	$ cd srv
 	$ java -jar BuildTools.jar --rev latest
Le serveur est normalement installé :
	$ sudo java -jar -Xms1024M -Xmx2048M spigot-1.x.x.jar nogui
Cette commande permet de lancer le serveur. Elle ne marchera pas car il faut d'abord accepter les conditions :
	$ sudo sed -i 's/false/true/g' /home/minecraft/srv/eula.txt
Le fichier serveur.properties est maintenant disponible, vous allez pouvoir l'éditer : https://minecraft-fr.gamepedia.com/Server.properties
	- Vous pouvez également changer le numéro de port (25565 par defaut) ou le nombre de joueurs max (dépend de la capacité de la machine qui héberge le serveur et de votre connexion).

C'est normalement bon, vous pouvez lancer le serveur :
	$ sudo java -jar -Xms1024M -Xmx2048M spigot-1.x.x.jar nogui
	- -Xms1024M et -Xmx2048M donnent la RAM min et max allouée, vous pouvez donc la modifier selon la capacité de votre machine
	- spigot-1.x.x où 1.x.x correspond à la version du jeu. Normalement, cette version est la dernière en date. Vous pouvez changer la version du jeu dans le Minecraft launcher afin de matcher la version du serveur. Vous le numéro de version avec la commande "find . -type f -iname "spigot-1.*"
	- nogui permet de ne pas avoir deux fenêtres. (terminal + serveur).
Deux commandes utiles :
	- help -> pour avoir toutes les commandes.
	- stop -> pour stopper le serveur.


# Rendre le serveur accéssible à tous
Pour jouer en local (tout le monde est connecté au même réseau internet), il faut taper la commande suivante sur la machine qui héberge le serveur :
	$ ifconfig
Le port local doit être visible sur un des reseaux. Il aura toujours la forme 192.168.x.x.
L'adresse du serveur est donc la suivante : 192.168.x.x:25565.

Pour ouvrir l'accès à l'extérieur, il faut paramétrer votre routeur pour ouvrir le port exact de votre serveur.
Voici un tuto pour la raspberry, mais il s'applique à tout type d'appareil : https://raspberry-pi.fr/mettre-en-ligne-serveur-web-raspbian-dydns-port-forwarding/
Attention, cette commande peut comporter des risque de sécurité, veuillez vous renseigner afin de prendre cette décision en toute connaissance de cause.
	- Ouvrir l'interface de votre modem. L'url change selon le fournisseur mais c'est très souvent le début des ip locales :
		192.168.1.1, 192.168.0.1, ...
	- Les instructions pour se connecter sont marquées sur l'interface.
	- Commencez par fixer une ip locale statique à votre machine : ça évitera les soucis à l'avenir.
		Sur la livebox, il s'agit de la section "Baux DHCP statiques" de la partie "Réseau/DHCP".
	- Ouvrez votre serveur à l'extérieur : Dans la partie "Réseau/NAT", ajoutez une règle personnalisée :
		Service: Web Server (HTTP)
		Port interne : 25565
		Port externe : 25565 (je conseille de garder le port par defaut)
		protocole : TCP
		Équipement: nom_de_votre_machine
Une sauvegarde, et le serveur est maintenant accéssible depuis l'extérieur.
Pour y accéder, vous devez connaitre votre ip publique: il s'agit de l'adresse ip de votre modem.
https://www.whatismyip.com/fr/ vous devez copier votre adresse ipv4 sous la forme A:B:C:D
	- A, B, C et D sont des chiffres compris entre 0 et 255
L'adresse du serveur est donc la suivante : A.B.C.D:25565.
PS : tant que vous êtes sur l'interface de gestion du modem, pensez à changer votre mot de passe wifi ;)

Pour rendre votre serveur accéssible à tous, il faut modifier le ficher server.properties.
Je ne suis pas responsable de ce que vous faites, mais sachez que c'est possible.
	- online-mode : false pour autoriser les versions craquée du jeu


# Créer une interface de gestion
Si comme moi vous voulez pour une raison ou une autre créer une interface de gestion, vous allez avoir besoin d'un outil : tmux https://doc.ubuntu-fr.org/tmux
Tmux (comme screen) est un outil qui permet de faire des sessions sur votre machine. La session reste active tant que la machine ne s'éteint pas, ou que l'utilisateur ne la tue pas.
Dans notre cas, la machine restant allumée h24, il nous faut un moyen d'ouvrir le terminal avec l'éventuelle session du serveur minecraft en cours: tmux permet de faire cela.
Une fois tmux installé, créez une session dédiée à votre serveur dans le dossier parent :
	$ cd ..
	$ ls -> doit afficher "app, srv, .gitignore, README.md"
	$ tmux new -s mcapp
Quittez la session avec ctrl+b, d.
	$ tmux ls -> doit vous afficher la session mcapp.
	$ tmux -a mcapp -> doit vous faire re-rentrer dans la session.
Une fois dans la session tmux, vous pouvez lancer le serveur à votre guise, puis quitter la session (sans la fermer). 
La prochaine fois que vous lancerez "tmux -a mcapp" vous entrerez dans la session et pourrez contrôler votre serveur facilement !

Placez vous dans le dossier app, utilisez un premier terminal pour lancer le front :
	$ cd app/front
	$ yarn
	$ yarn start
Il vous faudra modifier 2 fichiers pour connecter les 2 applis :
	- app/front/src/env/environments.js : export const server = 'A.B.C.D';
	- app/front/src/env/local.js : 
		-> importer server plutot que local à la ligne 1;
		-> export const backUrl = server;

Ouvrez un second terminal (ctrl+b, c), lancez le back :
	$ cd app/back
	$ npm i
	$ nodemon server.js
Nodemon permet d'executer une appli node. La commande est équivalente à "node" mais se relancera à chaque mise à jour : https://nodemon.io/

Ouvrez un troisième terminal (ctrl+b, c), pour préparer le terminal du serveur :
	$ cd srv
	$ echo 1.X.X > version.txt
	
Si vous avez gardé l'architecture que je vous ait proposé, le projet marche tout seul. Vous pouvez voir l'application sur http://localhost:3000 et contrôler votre serveur minecraft. Sinon, vous devrez adapter le code de la partie back pour qu'il matche votre configuration.
Vous pouvez diffusez l'appli pour que les autres joueurs puissent aussi se connecter, en faisant la manipulation décrite pour rendre le serveur minecraft accéssible à l'extérieur :
	- Dans la partie "Réseau/NAT" de la page de configuration de votre modem, ajoutez une règle personnalisée :
		Service: Web Server (HTTP)
		Port interne : 3019
		Port externe : 1337 (vous pouvez vous faire plaisir sur celui là)
		Protocole : TCP
		Équipement: nom_de_votre_machine
L'adresse de l'appli est donc la suivante : A.B.C.D:1337 (ou le port que vous aurez choisit #plaisir).
J'ai essayé de penser à tout, de laisser un minimum de chemin absolus pour que le code marche avec tout le monde, ça n'est pas si simple. Aussi j'ai laissé des logs un peu partout. Si vous constatez des erreurs, n'hésitez pas à regarder les logs dans tmux ou bien à me laisser une issue sur gitlab, j'essayerai d'y répondre ! 

# Fin


# English

# Introduction
This project allows to transform a linux xomputer into a minecraft server. It contains several steps :
	- Host the minecraft server itself.
	- Make it available to people outside of your network, but also to every version (Warning, theft is illegal, please buy your games).
	- Give the possibility to switch on and off your server though a web application (client + server).

FYI, in my case, the serveur is hosted on a Raspberry Pi 4 (4GB), which allow to grant a lot of ressources to the server and thus, host more players without overcharging your computer.
Also, the raspberry being on 24/7, the web app allows other people to play without my intervention to switch on and off the server (It consummes a lot .of energy, so, if my raspberry is on 24/7, it's kinda low consumming, which is not the case when minecraft is running)
The raspberry being on Raspbian, all the commands I will quote are valid on Debian distros.


# Host the Minecraft Server
This first step is the simpliest, there are a lot of tutorials online to do so :
https://minecraft.gamepedia.com/Tutorials/Server_startup_script
You first need to install the latest java version (change the jdk number to the latest) :
	$ sudo apt-get install openjdk-8-jdk
Then check your java version :
	$ java -version
>openjdk version "11.0.8" 2020-07-14
>OpenJDK Runtime Environment (build 11.0.8+10-post-Ubuntu-0ubuntu120.04)
>OpenJDK 64-Bit Server VM (build 11.0.8+10-post-Ubuntu-0ubuntu120.04, mixed mode, sharing

Split your project into 2 parts : I advise 2 splitted sub-folders
	- srv -> For the Minecraft server itself.
	- app -> To create the app that allows to controle your server (useless if you play locally).
Download the latest BuildTools version in the "srv" folder.
https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar
Launch :
	$ cd srv
 	$ java -jar BuildTools.jar --rev latest
The server should be installed :
	$ sudo java -jar -Xms1024M -Xmx2048M spigot-1.x.x.jar nogui
This command launches the server. It won't work because you first need the accept the conditions :
	$ sudo sed -i 's/false/true/g' /home/minecraft/server/eula.txt
The server.properties file is now available, you can edit it : https://minecraft.gamepedia.com/Server.properties
	- You can change the port number (25565 by default) or the max player number (depends on the capapcity of the host machine and your connection).

It should be ok, you can launch the server :
	$ sudo java -jar -Xms1024M -Xmx2048M spigot-1.x.x.jar nogui
	- Xms1024M et -Xmx2048M are the min and max granted RAM, you can modify it according to your appareil's capacity.
	- spigot-1.x.x where 1.x.x corresponds to the game version. It should be the latest. You can change the game version in the Minecraft launcher in order to match the server's one. You will find the version number with the command "find . -type f -iname "spigot-1.*"
	- nogui is here to avoid having a second window (terminal + server)
Two usefull commands :
	- help to see every commands
	- stop to stop the server (incredible)


# Make the server available to everyone
To play locally (everyone is connected to the same internet network), you need to run the following command on the host appareil :
	$ ifconfig
The local port should be visible on one of the netxworks. It always have the form 192.168.x.x
Thus, the server address is the following : 192.168.x.x:25565.

To grant the access to the outside, you need to parameter your router to open the exact port of your server.
Here is a raspberry tutorial, but it's the same for every appareil : https://janw.me/2017/opening-raspberry-pi-to-outside-world/
Warning, this command could involve security issues, please inquire to take this decision knowingly.
	- Open your router's interface. The url is not the same according to your internet provider, but it's often the beginning of local IP :
		192.168.0.0, 192.168.1.1, ...
	- Connection instructions are displayed on the interface.
	- Start to fix the local ip of your hosting machine : it will avoid future problems
		On my router, it's on the "Network/DHCP" part.
	- Open your server on the outside world : In the "Network, NAT" part, add a rule :
		Service: Web Server (HTTP)
		Interne port : 25565
		Externe port : 25565 (I advise to keep the default port)
		Protocol : TCP
		Equipment: name_of_your_machine
You can save, and the server is now available from the outside world.
To access it, you should know your pulic ip : It's your router's one.
https://www.whatismyip.com you should copy your ipv4 address under the form A:B:C:D
	- A, B, C et D are numbers betwwen 0 and 255.
The server's address is thus the following : A.B.C.D:25565

To make your server available to everyone, you need to modify the server.properties file.
I am not responsible of what you do, but you should know that it is possible.
	- online-mode: false to authorize the craked versions of the game


# Create a management interface
If, just like me you want for one reason or another create a management interface, you will need one tool : tmux https://doc.ubuntu-fr.org/tmux
Screen is a tool that allow you to make sessions on your machine. The session remains active until the appareil switchs off, or until the user kills it.
In our case, the machine being on 24/7, we need to open the terminal with the eventual session of the running minecraft server : tmux allows to do so.
Once tmux is installed, create a dedicated session to your server in the parent folder :
	$ cd ..
	$ ls -> should display "app, srv, .gitignore, README.md"
	$ tmux new -s mcapp
Quit the session with ctrl+b, d.
	$ tmux ls -> Should display the session "mcapp"
	$ tmux -a mcapp -> should make you re-join the session
Once you are in the tmux session, you can launch the server as you wish, and then quit the session (without closing it).
The next time you launch "tmux -a mcapp" you will enter in the session and will be able to control it easily !

You can then use another session, named "web-app" to manage your server remotely.
Go to the app folder, and then use a first terminal to launch the front :
	$ cd app/front
	$ yarn
	$ yarn start
You will have to modify 2 files to connect both apps :
	- app/front/src/env/environments.js : export const server = 'A.B.C.D';
	- app/front/src/env/local.js : 
		-> import server instead of local at line 1;
		-> export const backUrl = server;

Open a second terminal (ctrl+b, c), launch le back :
	$ cd app/back
	$ npm i
	$ nodemon server.js
Nodemon allows you to execute a node app. The command is equivalent to "node" but has a hotreload option : https://nodemon.io/

Open a third terminal (ctrl+b, c), to prepare the server's terminal
	$ cd srv
	$ echo 1.X.X > version.txt

If you kept the proposed architecture, the project works by itself. You can is the app on http://localhost:3000 and control the minecraft server.
Otherwise, you will need to adapt the back-end's part of the code so it matches your configuration.
You can diffuse the app so the other players can also control it, making the same manipulation described to make the minecraft server available world-wide :
	- In the "Network/NAT" part of your modem's configuration interface, add a custom rule :
		Service: Web Server (HTTP)
		Intern port : 3019
		Extern port : 1337 (You can have fun on this one)
		Protocol : TCP
		Equipment: name_of_your_machine
The address of the app is thus the following : A.B.C.D:1337 (or the port you chose #pleasure).
I tried to think about everything, to leave as less absolute path as possible so the code will work for everyone, but it's not that simple. I also left logs kinda everywhere. If you see errors, do not hesitate to look at these logs or to leave me an issue on gitlab, I'll try to answer it !

# End
